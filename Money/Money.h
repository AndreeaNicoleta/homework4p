#pragma once
#include<string>
class Money
{
private:
	int euros;
	int centimes;
public:
	Money(int euros, int centimes);
	~Money();
	int getEuros();
	void setEuros(int euros);
	int getCentimes();
	void setCentimes(int centimes);
	void print();
	Money operator+(Money a);
	Money operator-(Money a);
	Money operator*(Money a);
	Money operator*(int value);
	Money operator/(int value);
	bool operator!=(Money a);
	bool operator==(Money a);



};

