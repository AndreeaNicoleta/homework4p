#include<iostream>
#include "Money.h"

using namespace std;
void main()
{
	//Definesc doua obiecte distincte de tipul Money
	Money m(2, 99);
	Money m2(1, 0);

	//Scadere
	Money rez1 = m - m2;
	rez1.print();

	//Adunare
	Money rez2 = m + m2;
	rez2.print();
	
	//Adunarea multipla a obiectului m
	Money rez = m + m + m;
	rez.print();

	//Verific daca m e acelasi cu m2
	if (m.operator==(m2))
	{
		cout << "m "; 
		m.print();
		cout << " este identic cu m2 ";
		m2.print();
		cout << endl;
	}
	else {
		cout << "m ";
		m.print();
		cout << "NU este identic cu m2 ";
		m2.print();
		cout << endl;
	}

	//Verific daca m NU e acelasi cu m2
	if (m.operator!=(m2))
	{
		cout << "m ";
		m.print();
		cout << "NU este identic cu m2 ";
		m2.print();
		cout << endl;
	}
	else {
		cout << "m ";
		m.print();
		cout << "este identic cu m2 ";
		m2.print();
		cout << endl;
	}



	

}
