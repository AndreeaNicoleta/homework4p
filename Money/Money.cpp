#include "Money.h"
#include<iostream>
#include<string>

using namespace std;

Money::Money(int euros, int centimes)
{
	//Pentru a creea obiectul corect, adica sa se verifice daca 
	if (centimes >= 100)
	{
		int ordin = centimes / 100;
		this->euros = euros + ordin;
		this->centimes = centimes + 100 * ordin;
	}
	else
	{
		this->euros = euros;
		this->centimes = centimes;
	}


}


Money::~Money()
{
}

int Money::getEuros()
{
	return this->euros;
}

void Money::setEuros(int euros)
{
	this->euros = euros;
}

int Money::getCentimes()
{
	return this->centimes;
}

void Money::setCentimes(int centimes)
{
	this->centimes = centimes;
}

void Money::print()
{
	cout<<"euros: "<<this->getEuros() <<" centimes: "<<this->getCentimes()<<endl;

}

Money Money::operator+(Money a)
{
	Money rez(0,0);
	rez.setEuros(this->getEuros() + a.getEuros());
	rez.setCentimes(this->getCentimes() + a.getCentimes());
	//daca avem mai mult de 100 centi, crestem euro cu centimes/100=unitati, respectiv se va scadea din centimes 100* unitati
	if (rez.getCentimes() >= 100)
	{
		int ordin = rez.getCentimes() / 100;
		rez.setCentimes(rez.getCentimes() - ordin * 100);
		rez.setEuros(rez.getEuros() + ordin);
	}
	return rez;

}

Money Money::operator-(Money a)
{
	Money rez(0, 0);
	int borrow = 0;
	int finalCents = this->getCentimes() - a.getCentimes();
	if (finalCents < 0)
	{
		//ne imprumutam cu 100 de centimes, echivalentul unui euro
		finalCents += 100;
		//variabila de imprumut
		borrow = 1;
	}
	//euro final, va avea cu 1 mai putin, din cauza imprumutului
	int finalEuros = this->getEuros() - a.getEuros() - borrow;
	rez.setEuros(finalEuros);
	rez.setCentimes(finalCents);

	return rez;

}

Money Money::operator*(Money a)
{
	Money rez(0, 0);
	rez.setEuros(this->getEuros() * a.getEuros());
	rez.setCentimes(this->getCentimes() * a.getCentimes());
	if (rez.getCentimes() >= 100)
	{
		int ordin = rez.getCentimes() / 100;
		rez.setCentimes(rez.getCentimes() - ordin * 100);
		rez.setEuros(rez.getEuros() + ordin);
	}
	return rez;
}

Money Money::operator*(int value)
{
	Money rez(0, 0);
	rez.setEuros(this->getEuros() * value);
	rez.setCentimes(this->getCentimes() * value);
	if (rez.getCentimes() >= 100)
	{
		int ordin = rez.getCentimes() / 100;
		rez.setCentimes(rez.getCentimes() - ordin * 100);
		rez.setEuros(rez.getEuros() + ordin);
	}
	return rez;
}

Money Money::operator/(int value)
{
	Money rez(0, 0);
	rez.setEuros(this->getEuros() / value);
	rez.setCentimes(this->getCentimes() / value);

	return rez;

}

bool Money::operator!=(Money a)
{
	
	if (this->getEuros() != a.getEuros() || this->getCentimes() != a.getCentimes())
		return true;
	return false;


}

bool Money::operator==(Money a)
{

	if (this->getEuros() == a.getEuros() && this->getCentimes() == a.getCentimes())
		return true;
	return false;


}