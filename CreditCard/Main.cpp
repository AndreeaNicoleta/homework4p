#include<iostream>
#include "CreditCard.h"
#include "Money.h"

using namespace std;

void main()
{
	CreditCard card = CreditCard("Elena", "myCard");
	card.add1("Tranzactie1", Money(20, 55));
	card.add1("Tranzactie2", Money(200, 550));
	card.add2("Tranzactie3", 200, 500);
	card.add2("Tranzactie4", 100, 20);
	cout<<card.toString();
}