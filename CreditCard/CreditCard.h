#pragma once
#include "Money.h"
#include<string>
#include<map>
using namespace std;
class CreditCard
{
private:
	string ownerName;
	string cardNumber;
	map< string,  Money> tranzaction;
public:
	CreditCard(string ownerName, string cardNumber);
	~CreditCard();
	void add1(string name, Money money);
	void add2(string name, int euro, int centimes);
	string toString();
	
};

