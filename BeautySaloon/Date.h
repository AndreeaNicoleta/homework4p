#pragma once
#include <ctime>
class Date
{
private:
	int day;
	int mounth;
	int year;
	int hour;
	int minute;

public:
	Date();
	Date(int year, int mounth, int day, int hour, int minute);
	~Date();
	time_t getTime();
	void setTime(time_t t);
};

