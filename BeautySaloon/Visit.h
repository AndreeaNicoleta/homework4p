#pragma once
#include "Customer.h"
#include<string>
#include "Date.h"

using namespace std;
class Visit
{
private:
	Customer customer;
	Date date;
	double serviceExpense;
	double productExpense;
public:
	Visit();
	Visit(string name, Date date);
	string getName();
	double getServiceExpense();
	void setServiceExpense(double value);
	double getProductExpense();
	void setProductExpense(double value);
	double getTotalExpense();
	string toString();
	Customer getCustomer();

	ostream& operator << (ostream& flux);
	~Visit();
};

