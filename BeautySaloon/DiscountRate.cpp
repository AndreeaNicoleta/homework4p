#include "DiscountRate.h"



DiscountRate::DiscountRate()
{
}


DiscountRate::~DiscountRate()
{
}

double DiscountRate::getServiceDiscountRate(string type)
{
	if (type.compare("Premium") == 0)
		return this->serviceDiscoutPremium;
	else if (type.compare("Gold") == 0)
		return this->serviceDiscoutGold;
	else if (type.compare("Silver") == 0)
		return this->serviceDiscoutSilver;
}

double DiscountRate::getProductDiscountRate(string type)
{

	if (type.compare("Premium") == 0)
		return this->productDiscoutPremium;
	else if (type.compare("Gold") == 0)
		return this->productDiscoutGold;
	else if (type.compare("Silver") == 0)
		return this->productDiscoutSilver;
}
