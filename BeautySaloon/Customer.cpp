#include "Customer.h"
#include <string>


Customer::Customer()
{
}

Customer::Customer(string name)
{
	this->name = name;
}

Customer::~Customer()
{
}

string Customer::getName()
{
	return this->name;
}


bool Customer::isMember()
{
	return member;
}

void Customer::setMember(bool member)
{
	this->member = member;
}

string Customer::getMemberType()
{
	return this->memberType;
}

void Customer::setMemberType(string type)
{
	this->memberType = type;
}

string Customer::toString()
{
	return "name: "+this->getName()+", memberType: "+this->getMemberType();
}
