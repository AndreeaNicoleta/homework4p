#include<iostream>
#include"Customer.h"
#include"Visit.h"
#include"DiscountRate.h"
#include<vector>


using namespace std;
void main()
{
	/*Visit *visits = new Visit[9];
	for(int i=1; i<=9; i++)
		visits[i].*/
	Visit visit1 = Visit("Simona", Date(2017, 4, 11, 17, 30));
	Visit visit2 = Visit("Simona", Date(2017, 3, 17, 10, 00));
	Visit visit3 = Visit("Simona", Date(2016, 12, 20, 12, 45));
	Visit visit4 = Visit("Raluca", Date(2017, 4, 11, 17, 30));
	Visit visit5 = Visit("Raluca", Date(2016, 12, 10, 16, 30));
	Visit visit6 = Visit("Andreea", Date(2017, 2, 15, 17, 30));
	Visit visit7 = Visit("Simona", Date(2017, 3, 21, 17, 30));
	Visit visit8 = Visit("Cristina", Date(2017, 3, 21, 17, 30));
	Visit visit9 = Visit("Cristina", Date(2017, 1, 5, 20, 30));
	//vector<Visit> v;
	vector<Visit> visits(9);
	visits.push_back(visit1);
	visits.push_back(visit2);
	visits.push_back(visit3);
	visits.push_back(visit4);
	visits.push_back(visit5);
	visits.push_back(visit6);
    visits.push_back(visit7);
	visits.push_back(visit8);
	visits.push_back(visit9);


	Customer client1 = Customer("Simona");
	Customer client2 = Customer("Raluca");
	Customer client3 = Customer("Andreea");
	Customer client4 = Customer("Cristina");
	
	client1.setMember(true);
	client2.setMember(true);
	client3.setMember(false);
	client4.setMember(true);
	client1.setMemberType("Gold");
	client2.setMemberType("Silver");
	client4.setMemberType("Premium");
	visit1.setProductExpense(200);
	visit1.setServiceExpense(200);
	visit2.setProductExpense(0);
	visit2.setServiceExpense(100);
	visit3.setProductExpense(240.8);
	visit3.setServiceExpense(200.23);
	visit4.setProductExpense(10);
	visit4.setServiceExpense(100.76);
	visit5.setProductExpense(120.77);
	visit5.setServiceExpense(0);
	visit6.setProductExpense(0);
	visit6.setServiceExpense(100.66);
	visit7.setProductExpense(90);
	visit7.setServiceExpense(90);
	visit8.setProductExpense(10.909);
	visit8.setServiceExpense(100.66);
	visit9.setProductExpense(12);
	visit9.setServiceExpense(78);

	DiscountRate discount = DiscountRate();
	cout << "Visits: ";
	for (int i = 0; i < visits.size(); i++)
		cout << visits.at(i).toString() << endl;

	for (Visit visit : visits)
	{
		if (visit.getCustomer().isMember())
			if (visit.getCustomer().getMemberType().compare("Premium"))
			{
				visit.setProductExpense(visit.getProductExpense() - visit.getProductExpense()*discount.getProductDiscountRate("Premium"));
				visit.setServiceExpense(visit.getServiceExpense() - visit.getServiceExpense()*discount.getServiceDiscountRate("Premium"));
			}
			else if (visit.getCustomer().getMemberType().compare("Gold"))
			{
				visit.setProductExpense(visit.getProductExpense() - visit.getProductExpense()*discount.getProductDiscountRate("Gold"));
				visit.setServiceExpense(visit.getServiceExpense() - visit.getServiceExpense()*discount.getServiceDiscountRate("Gold"));
			}
			else if (visit.getCustomer().getMemberType().compare("Silver"))
			{
				visit.setProductExpense(visit.getProductExpense() - visit.getProductExpense()*discount.getProductDiscountRate("Silver"));
				visit.setServiceExpense(visit.getServiceExpense() - visit.getServiceExpense()*discount.getServiceDiscountRate("Silver"));
			}
	}

	cout << "Visits after discount: ";
	for (int i = 0; i < visits.size(); i++)
		cout << visits.at(i).toString() << endl;



}