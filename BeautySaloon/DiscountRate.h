#pragma once
#include<string>
using namespace std;
class DiscountRate
{
private:
	const double serviceDiscoutPremium = 0.2;
	const double serviceDiscoutGold = 0.15;
	const double serviceDiscoutSilver = 0.1;

	const double productDiscoutPremium = 0.1;
	const double productDiscoutGold = 0.1;
	const double productDiscoutSilver = 0.1;

public:
	DiscountRate();
	~DiscountRate();
	double getServiceDiscountRate(string type);
	double getProductDiscountRate(string type);
};

