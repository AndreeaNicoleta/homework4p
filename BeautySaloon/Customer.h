#pragma once
#include <string>
using namespace std;
class Customer
{
private:
	string name;
	bool member = false;
	string memberType;
public:
	Customer();
	Customer(string name);
	~Customer();
	string getName();
	void getName(string name);
	bool isMember();
	void setMember(bool member);
	string getMemberType();
	void setMemberType(string type);
	string toString();
};

